import Head from "next/head";
import Image from "next/image";

export default function Home() {
  return (
    <div
      className={`d-flex flex-column justify-content-around align-items-center`}
    >
      <style jsx>{``}</style>
      <div className={`mx-auto`}>
        <p className={`h1`}>NIDUS.</p>
        <p className={`h-3`}></p>
        <p className={`h-4`}></p>
      </div>
      <div>
        <a target='_blank' href="https://matrix.to/#/!cvmhaJRqLcWTobRDjp:matrix.org?via=matrix.org">
          Join The Coimmunity
        </a>
      </div>
    </div>
  );
}
