import App from "next/app";
import React from "react";
import { Provider } from "react-redux";
import { store } from "../store";
import Head from "next/head";
import "bootstrap/dist/css/bootstrap.min.css";
import metrics from "../metrics";

import Layout from "../components/common/layout";
/**
 * Handles the Vitals and Metrics of the Webapp
 * @param {Object} metric
 */
export const reportWebVitals = (metric) => metrics(metric);

function MyApp({ Component, pageProps }) {
  return (
    <div className="h-100">
      <Head>
        <meta
          name="viewport"
          content="initial-scale=1.0, width=device-width"
          key="viewport"
        />
        <title>NIDUS.</title>
      </Head>

      <div className="h-100">
        <Provider session={pageProps.session} store={store}>
          {/* <ThemeProvider theme={theme}> */}
          <Layout>
            <Component {...pageProps} />
          </Layout>
          {/* </ThemeProvider> */}
        </Provider>
      </div>
    </div>
  );
}

export default MyApp;
