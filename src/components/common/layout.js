import { useState, useEffect } from "react";
import { connect } from "react-redux";
import Navbar from "./navbar";
import Sidebar from "./sidebar";
import LoginModal from "../loginModal";
import { init } from "../../actions/web3Actions";

function layout({ children, layoutContainerStyle = "", init }) {
  const [show, setShow] = useState(false);
  const [showSidebar, setShowSidebar] = useState(false);
  useEffect(() => {
    setShowSidebar(!window.location.pathname === "/");
  }, []);
  // INITS WEB3 Functionality
  useEffect(() => {
    init();
  }, []);

  return (
    <div className={`d-flex flex-column h-100 w-100 fnt-monospace`}>
      <style jsx global>
        {`
          .content-wrapper {
            height: calc(100% - 36px);
            width: 100%;
          }
        `}
      </style>
      {/* NAVBAR */}
      <Navbar
        navbarContainerStyle={"sticky-top border-top border-bottom border-dark"}
        brandText={process.env.BRAND_NAME||'NIDUS.'}
        brandTextStyle={`text-uppercase`}
        onClick={() => setShow(!show)}
      />
      <div className={`content-wrapper contianer-fluid d-flex flex-row`}>
        {/**Side bar */}
        {showSidebar && <Sidebar sidebarContainerStyle={`fnt-monospace`} />}
        {/**Main Content */}
        <main className={`p-2 w-100 ${layoutContainerStyle}`}>{children}</main>
      </div>
      {/* LOGIN MODAL */}
      {show && <LoginModal handleShow={() => setShow(!show)} />}
    </div>
  );
}
export default connect(null, { init })(layout);
