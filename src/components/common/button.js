export default function Button({
  children,
  buttonStyle = "",
  onPress = () => {},
}) {
  return (
    <button onClick={() => onPress()} className={`${buttonStyle} btn`}>
      {children}
    </button>
  );
}
