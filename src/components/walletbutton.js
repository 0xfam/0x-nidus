import { useState } from "react";
import { truncateAddress } from "../utility/moiWeb3";
import WalletButtonItem from "./walletbuttonitem";
function walletbutton({ address = "", onPress = () => {} }) {
  const [show, setShow] = useState(false);
  return (
    <div className={`h-100 border-start border-dark wallet-button `}>
      <style>
        {`
        .wallet-button{  
          width: 175px;
        }
        .wallet-button-items{
          max-height:18.75rem;
          width: 175px;
        }
        `}
      </style>
      {address.length > 0 ? (
        <div
          className={`wallet-button-address hover-blackflame d-flex flex-column align-items-center justify-content-center px-3 h-100 w-100 cursor-point`}
          onClick={() => {
            setShow(!show);
          }}
        >
          {truncateAddress(address)}
        </div>
      ) : (
        <div
          className={`wallet-button-address hover-blackflame d-flex flex-column align-items-center justify-content-center px-3 h-100 w-100 cursor-point`}
          onClick={() => {
            onPress();
          }}
        >
          CONNECT
        </div>
      )}
      {show && (
        <div
          className={`wallet-button-items d-flex flex-column px-2 mr-1 bg-grey`}
        >
          <WalletButtonItem text={`Connect Profile...`} />
        </div>
      )}
    </div>
  );
}
export default walletbutton;
