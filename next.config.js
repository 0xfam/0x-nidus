const withPWA = require("next-pwa");

const nextConfig = {
  pwa: {
    dest: "public",
    
  },
  devIndicators: {
    autoPrerender: true,
  },
  webpackDevMiddleware: (config) => {
    // Solve compiling problem via vagrant
    config.watchOptions = {
      poll: 1000, // Check for changes every second
      aggregateTimeout: 300, // delay before rebuilding
    };
    return config;
  },
  async rewrites() {
    return [];
  },
  env: {
    INFURA_API: process.env.INFURA_API,
    BRAND_NAME: process.env.BRAND_NAME,
  },
};

module.exports = withPWA(nextConfig);
